import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class FirstGet {
    private String username = "ck_8da07afd3503a4242e95a2d5bad99cc2fb55c552";
    private String password = "cs_e57e8cadc42ca42064c44239853dda9aa2e77474";
    private String url = "http://localhost/fakestore/wp-json/wc/v3/";
    private String productsEndpoint = "products";
    private String productFuerta = "393";
    int valueId;


    @Test
    public void afirstPostTest() {
        System.out.println("startuje metoda post");
        String body = "{\"name\":\"Created in RestAssured\"}";
        Response response = given()
                .port(80)
                .auth()
                .oauth(username, password,"","")
                .contentType("application/json")
                .log().all()
                .body(body)
                .post(url + productsEndpoint);
        valueId = response.jsonPath().get("id");
        Assertions.assertEquals(201, response.statusCode());
        System.out.println("wartosc valueID: " + valueId );
        //System.out.println(response.jsonPath().get("id"));
    }

    @Test
    public void bfirstGetTest() {
        System.out.println("startuje metoda get");
        System.out.println("wartosc valueId " + valueId);

        Response response = given()
                .port(80)
                .auth()
                .oauth(username, password, "", "")
                //.log().all()
                .when()
                .get(url + productsEndpoint + "/" + valueId);

       // System.out.println(response.prettyPeek());
       // System.out.println(response.getBody().prettyPrint());
//        System.out.println(response.jsonPath().get("name"));
        Assertions.assertEquals(200, response.statusCode());
    }


//    @Test
//    public void firstDeletetTest() {
//        Response response = given()
//                .port(80)
//                .auth()
//                .oauth(username, password,"","")
//                .contentType("application/json")
//                .log().all()
//                .delete(url + productsEndpoint + "/" + productCreated);
//
//        Assertions.assertEquals(200, response.statusCode());
//    }
//
//    @Test
//    public void firstPutTest() {
//        String body = "{\"name\":\"Update in 156123RestAssured LM\"}";
//        Response response = given()
//                .port(80)
//                .auth()
//                .oauth(username, password,"","")
//                .contentType("application/json")
//                .log().all()
//                .body(body)
//                .put(url + productsEndpoint + "/" + productCreated);
//
//        Assertions.assertEquals(200, response.statusCode());
//        // System.out.println(response.jsonPath().get("id"));
//    }
//
//    @Test
//    public void firstPatchTest() {
//        String body = "{\"name\":\"Update PATH432543654 in RestAssured\"}";
//        Response response = given()
//                .port(80)
//                .auth()
//                .oauth(username, password,"","")
//                .contentType("application/json")
//                .log().all()
//                .body(body)
//                .patch(url + productsEndpoint + "/" + productCreated);
//
//        //Assertions.assertEquals(200, response.statusCode());
//        // System.out.println(response.jsonPath().get("id"));
//    }

}
